/* eslint-disable max-len */
import { expect } from 'chai'; // eslint-disable-line import/no-extraneous-dependencies
import Compel from '../dist/compel.api.min.js';
import exampleOne from './resources/example_one.json';
import exampleTwo from './resources/example_two.json';
import exampleThree from './resources/example_three.json';
import exampleFour from './resources/example_four.json';
import exampleFive from './resources/example_five.json';


/*const compel = new Compel();
compel.fillMissingFeatures(exampleFive).then(res => (
	console.log(JSON.stringify(res))
));*/

describe(`compel - testing ${(typeof window !== 'undefined') ? 'browser' : 'node'}`, () => {
	const compel = new Compel();
	describe('#checkMissingFeatures', () => {
		describe('Testing on test/resources/example_one.js', () => {
			const completitionResults1 = compel.checkMissingFeatures(exampleOne);
			it('2 features should be missing', () => {
				expect(completitionResults1.length).to.equal(2);
			});
			it('The missing features should be "date-doc" and "sector"', () => {
				expect(completitionResults1[0]).to.equal('date-doc');
				expect(completitionResults1[1]).to.equal('sector');
			});
		});
		describe('Testing on test/resources/example_two.js', () => {
			const completitionResults2 = compel.checkMissingFeatures(exampleTwo);
			it('3 features should be missing', () => {
				expect(completitionResults2.length).to.equal(3);
			});
			it('The missing feature should be "date-doc", "type" and "sector"', () => {
				expect(completitionResults2[0]).to.equal('type');
				expect(completitionResults2[1]).to.equal('date-doc');
				expect(completitionResults2[2]).to.equal('sector');
			});
		});
		describe('Testing on test/resources/example_three.js', () => {
			const completitionResults3 = compel.checkMissingFeatures(exampleThree);
			it('1 feature should be missing', () => {
				expect(completitionResults3.length).to.equal(1);
			});
			it('The missing features should be "sector"', () => {
				expect(completitionResults3[0]).to.equal('sector');
			});
		});
		describe('Testing on test/resources/example_four.js', () => {
			const completitionResults4 = compel.checkMissingFeatures(exampleFour);
			it('1 features should be missing', () => {
				expect(completitionResults4.length).to.equal(1);
			});
			it('The missing feature should be "sector"', () => {
				expect(completitionResults4[0]).to.equal('sector');
			});
		});
		describe('Testing on test/resources/example_five.js', () => {
			const completitionResults4 = compel.checkMissingFeatures(exampleFive);
			it('4 features should be missing', () => {
				expect(completitionResults4.length).to.equal(4);
			});
			it('The missing feature should be "type", "case-number", "case-year" and  "sector"', () => {
				expect(completitionResults4[0]).to.equal('type');
				expect(completitionResults4[1]).to.equal('case-number');
				expect(completitionResults4[2]).to.equal('case-year');
				expect(completitionResults4[3]).to.equal('sector');
			});
		});
	});

	describe('#fillMissingFeatures', () => {
		describe('Testing on test/resources/example_one.js', () => {
			let newJsonLd;
			let newJsonLd1;
			let features;
			let missingAfterCompletition1;
			before(async () => {
				newJsonLd = await compel.fillMissingFeatures(exampleOne);
				newJsonLd1 = newJsonLd[0];
				features = newJsonLd1.references[0].features;
				missingAfterCompletition1 = compel.checkMissingFeatures(newJsonLd1);
			});
			it('0 features should be missing after having competed the JSON-LD', () => {
				expect(missingAfterCompletition1.length).to.equal(0);
			});
			it('The confidence of the document type previously inserted in the document should be now equal to 0.1', () => {
				expect(features[1].confidence).to.equal('0.1');
			});
			it('The date of the document should be "2003-06-12" and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features[7].levels[0].values).to.equal('2003');
				expect(features[7].levels[1].values).to.equal('06');
				expect(features[7].levels[2].values).to.equal('12');
				expect(features[7].confidence).to.equal('1');
				expect(features[7].frame).to.equal('cellar');
			});
			it('The sector of the document should be 6 and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features[6].levels.values).to.equal('6');
				expect(features[6].confidence).to.equal('1');
				expect(features[6].frame).to.equal('cellar');
			});
			it('The new document type inserted should be "cdm:judgement" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[8].levels.values).to.equal('cdm:judgement');
				expect(features[8].confidence).to.equal('1');
				expect(features[8].frame).to.equal('cellar');
			});
			it('The new celex inserted should be "62000CJ0112" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[9].levels.values).to.equal('62000CJ0112');
				expect(features[9].confidence).to.equal('1');
				expect(features[9].frame).to.equal('cellar');
			});
			it('The new ecli inserted should be "ECLI:EU:C:2003:333" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[10].levels.values).to.equal('ECLI:EU:C:2003:333');
				expect(features[10].confidence).to.equal('1');
				expect(features[10].frame).to.equal('cellar');
			});
			it('The new cellar uri inserted should be "http://publications.europa.eu/resource/cellar/3a753d5f-e389-4f35-aeb1-73fb1422cfd4" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[11].levels.values).to.equal('http://publications.europa.eu/resource/cellar/3a753d5f-e389-4f35-aeb1-73fb1422cfd4');
				expect(features[11].confidence).to.equal('1');
				expect(features[11].frame).to.equal('cellar');
			});
		});

		describe('Testing on test/resources/example_two.js', () => {
			let newJsonLd;
			let newJsonLd1;
			let newJsonLd2;
			let features1;
			let features2;
			let missingAfterCompletition1;
			let missingAfterCompletition2;
			before(async () => {
				newJsonLd = await compel.fillMissingFeatures(exampleTwo);
				newJsonLd1 = newJsonLd[0];
				newJsonLd2 = newJsonLd[1];
				features1 = newJsonLd1.references[0].features;
				features2 = newJsonLd2.references[0].features;
				missingAfterCompletition1 = compel.checkMissingFeatures(newJsonLd1);
				missingAfterCompletition2 = compel.checkMissingFeatures(newJsonLd2);
			});
			it('0 features should be missing after having competed the JSON-LD (First Document)', () => {
				expect(missingAfterCompletition1.length).to.equal(0);
			});
			it('0 features should be missing after having competed the JSON-LD (Second Document)', () => {
				expect(missingAfterCompletition2.length).to.equal(0);
			});
			it('The sector of the first document should be 6 and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features1[5].levels.values).to.equal('6');
				expect(features1[5].confidence).to.equal('1');
				expect(features1[5].frame).to.equal('cellar');
			});
			it('The date of the first document should be "2002-06-13" and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features1[6].levels[0].values).to.equal('2002');
				expect(features1[6].levels[1].values).to.equal('06');
				expect(features1[6].levels[2].values).to.equal('13');
				expect(features1[6].confidence).to.equal('1');
				expect(features1[6].frame).to.equal('cellar');
			});
			it('The first document type inserted of the first document should be "cdm:document_cjeu" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[7].levels.values).to.equal('cdm:document_cjeu');
				expect(features1[7].confidence).to.equal('1');
				expect(features1[7].frame).to.equal('cellar');
			});
			it('The first document type inserted of the first document should be "cdm:judgement" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[8].levels.values).to.equal('cdm:judgement');
				expect(features1[8].confidence).to.equal('1');
				expect(features1[8].frame).to.equal('cellar');
			});
			it('The first celex inserted of the first document should be "62000CJ0117" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[9].levels.values).to.equal('62000CJ0117');
				expect(features1[9].confidence).to.equal('1');
				expect(features1[9].frame).to.equal('cellar');
			});
			it('The first ecli inserted of the first document should be "ECLI:EU:C:2002:366" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[10].levels.values).to.equal('ECLI:EU:C:2002:366');
				expect(features1[10].confidence).to.equal('1');
				expect(features1[10].frame).to.equal('cellar');
			});
			it('The first cellar uri inserted of the first document should be "http://publications.europa.eu/resource/cellar/efc02101-cf1c-4b72-bf1b-2cb618b83338" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[11].levels.values).to.equal('http://publications.europa.eu/resource/cellar/efc02101-cf1c-4b72-bf1b-2cb618b83338');
				expect(features1[11].confidence).to.equal('1');
				expect(features1[11].frame).to.equal('cellar');
			});
			it('The sector of the second document should be 6 and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features2[5].levels.values).to.equal('6');
				expect(features2[5].confidence).to.equal('1');
				expect(features2[5].frame).to.equal('cellar');
			});
			it('The date of the second document should be "2002-06-12" and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features2[6].levels[0].values).to.equal('2002');
				expect(features2[6].levels[1].values).to.equal('03');
				expect(features2[6].levels[2].values).to.equal('07');
				expect(features2[6].confidence).to.equal('1');
				expect(features2[6].frame).to.equal('cellar');
			});
			it('The first document type inserted of the second document should be "cdm:document_cjeu" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[7].levels.values).to.equal('cdm:document_cjeu');
				expect(features2[7].confidence).to.equal('1');
				expect(features2[7].frame).to.equal('cellar');
			});
			it('The second document type inserted of the second document should be "cdm:opinion_advocate-general" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[8].levels.values).to.equal('cdm:opinion_advocate-general');
				expect(features2[8].confidence).to.equal('1');
				expect(features2[8].frame).to.equal('cellar');
			});
			it('The second celex inserted of the second document should be "62000CC0117" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[9].levels.values).to.equal('62000CC0117');
				expect(features2[9].confidence).to.equal('1');
				expect(features2[9].frame).to.equal('cellar');
			});
			it('The second ecli inserted of the second document should be "ECLI:EU:C:2002:148" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[10].levels.values).to.equal('ECLI:EU:C:2002:148');
				expect(features2[10].confidence).to.equal('1');
				expect(features2[10].frame).to.equal('cellar');
			});
			it('The second cellar uri inserted of the second document should be "http://publications.europa.eu/resource/cellar/a646a507-b929-4b64-a54a-025c20819f85" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[11].levels.values).to.equal('http://publications.europa.eu/resource/cellar/a646a507-b929-4b64-a54a-025c20819f85');
				expect(features2[11].confidence).to.equal('1');
				expect(features2[11].frame).to.equal('cellar');
			});
		});
		describe('Testing on test/resources/example_three.js', () => {
			let newJsonLd;
			let newJsonLd1;
			let features;
			let missingAfterCompletition1;
			before(async () => {
				newJsonLd = await compel.fillMissingFeatures(exampleThree);
				newJsonLd1 = newJsonLd[0];
				features = newJsonLd1.references[0].features;
				missingAfterCompletition1 = compel.checkMissingFeatures(newJsonLd1);
			});
			it('0 features should be missing after having competed the JSON-LD', () => {
				expect(missingAfterCompletition1.length).to.equal(0);
			});
			it('The confidence of the document type previously inserted in the document should be now equal to 0.1', () => {
				expect(features[1].confidence).to.equal('0.1');
			});
			it('The date previously inserted in the document should be "2002-06-13" and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features[2].levels[0].values).to.equal('2002');
				expect(features[2].levels[1].values).to.equal('06');
				expect(features[2].levels[2].values).to.equal('13');
				expect(features[2].confidence).to.equal('1');
			});
			it('The sector of the document should be 6 and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features[7].levels.values).to.equal('6');
				expect(features[7].confidence).to.equal('1');
				expect(features[7].frame).to.equal('cellar');
			});
			it('The new document type inserted should be "cdm:judgement" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[8].levels.values).to.equal('cdm:judgement');
				expect(features[8].confidence).to.equal('1');
				expect(features[8].frame).to.equal('cellar');
			});
			it('The new celex inserted should be "62000CJ0117" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[9].levels.values).to.equal('62000CJ0117');
				expect(features[9].confidence).to.equal('1');
				expect(features[9].frame).to.equal('cellar');
			});
			it('The new ecli inserted should be "ECLI:EU:C:2002:366" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[10].levels.values).to.equal('ECLI:EU:C:2002:366');
				expect(features[10].confidence).to.equal('1');
				expect(features[10].frame).to.equal('cellar');
			});
			it('The new cellar uri inserted should be "http://publications.europa.eu/resource/cellar/efc02101-cf1c-4b72-bf1b-2cb618b83338" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[11].levels.values).to.equal('http://publications.europa.eu/resource/cellar/efc02101-cf1c-4b72-bf1b-2cb618b83338');
				expect(features[11].confidence).to.equal('1');
				expect(features[11].frame).to.equal('cellar');
			});
		});

		describe('Testing on test/resources/example_four.js', () => {
			let newJsonLd;
			let newJsonLd1;
			let features;
			let missingAfterCompletition1;
			before(async () => {
				newJsonLd = await compel.fillMissingFeatures(exampleFour);
				newJsonLd1 = newJsonLd[0];
				features = newJsonLd1.references[0].features;
				missingAfterCompletition1 = compel.checkMissingFeatures(newJsonLd1);
			});
			it('0 features should be missing after having competed the JSON-LD', () => {
				expect(missingAfterCompletition1.length).to.equal(0);
			});
			it('The confidence of the document type previously inserted in the document should be now equal to 0.1', () => {
				expect(features[1].confidence).to.equal('0.1');
			});
			it('The date previously inserted in the document should be "2002-06-12" and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features[2].levels[0].values).to.equal('2002');
				expect(features[2].levels[1].values).to.equal('06');
				expect(features[2].levels[2].values).to.equal('12');
				expect(features[2].confidence).to.equal('0.1');
			});
			it('The sector of the document should be 6 and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features[7].levels.values).to.equal('6');
				expect(features[7].confidence).to.equal('1');
				expect(features[7].frame).to.equal('cellar');
			});
			it('The new date inserted in the document should be "2002-06-13" and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features[8].levels[0].values).to.equal('2002');
				expect(features[8].levels[1].values).to.equal('06');
				expect(features[8].levels[2].values).to.equal('13');
				expect(features[8].confidence).to.equal('1');
			});
			it('The new document type inserted should be "cdm:judgement" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[9].levels.values).to.equal('cdm:judgement');
				expect(features[9].confidence).to.equal('1');
				expect(features[9].frame).to.equal('cellar');
			});
			it('The new celex inserted should be "62000CJ0117" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[10].levels.values).to.equal('62000CJ0117');
				expect(features[10].confidence).to.equal('1');
				expect(features[10].frame).to.equal('cellar');
			});
			it('The new ecli inserted should be "ECLI:EU:C:2002:366" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[11].levels.values).to.equal('ECLI:EU:C:2002:366');
				expect(features[11].confidence).to.equal('1');
				expect(features[11].frame).to.equal('cellar');
			});
			it('The new cellar uri inserted should be "http://publications.europa.eu/resource/cellar/efc02101-cf1c-4b72-bf1b-2cb618b83338" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features[12].levels.values).to.equal('http://publications.europa.eu/resource/cellar/efc02101-cf1c-4b72-bf1b-2cb618b83338');
				expect(features[12].confidence).to.equal('1');
				expect(features[12].frame).to.equal('cellar');
			});
		});

		describe('Testing on test/resources/example_five.js', () => {
			let newJsonLd;
			let newJsonLd1;
			let newJsonLd2;
			let features1;
			let features2;
			let missingAfterCompletition1;
			let missingAfterCompletition2;
			before(async () => {
				newJsonLd = await compel.fillMissingFeatures(exampleFive);
				newJsonLd1 = newJsonLd[0];
				newJsonLd2 = newJsonLd[1];
				features1 = newJsonLd1.references[0].features;
				features2 = newJsonLd2.references[0].features;
				missingAfterCompletition1 = compel.checkMissingFeatures(newJsonLd1);
				missingAfterCompletition2 = compel.checkMissingFeatures(newJsonLd2);
			});
			it('0 features should be missing after having competed the JSON-LD (First Document)', () => {
				expect(missingAfterCompletition1.length).to.equal(0);
			});
			it('0 features should be missing after having competed the JSON-LD (Second Document)', () => {
				expect(missingAfterCompletition2.length).to.equal(0);
			});
			it('The sector of the first document should be 6 and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features1[7].levels.values).to.equal('6');
				expect(features1[7].confidence).to.equal('1');
				expect(features1[7].frame).to.equal('cellar');
			});
			it('The date of the first document should be "2007-11-08" and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features1[2].levels[0].values).to.equal('2007');
				expect(features1[2].levels[1].values).to.equal('11');
				expect(features1[2].levels[2].values).to.equal('08');
				expect(features1[2].confidence).to.equal('1');
				expect(features1[2].frame).to.equal('extra');
			});
			it('The first document type inserted of the first document should be "cdm:document_cjeu" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[8].levels.values).to.equal('cdm:document_cjeu');
				expect(features1[8].confidence).to.equal('1');
				expect(features1[8].frame).to.equal('cellar');
			});
			it('The first document type inserted of the first document should be "cdm:judgement" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[9].levels.values).to.equal('cdm:judgement');
				expect(features1[9].confidence).to.equal('1');
				expect(features1[9].frame).to.equal('cellar');
			});
			it('The first celex inserted of the first document should be "62005CJ0020" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[10].levels.values).to.equal('62005CJ0020');
				expect(features1[10].confidence).to.equal('1');
				expect(features1[10].frame).to.equal('cellar');
			});
			it('The first ecli inserted of the first document should be "ECLI:EU:C:2007:652" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[11].levels.values).to.equal('ECLI:EU:C:2007:652');
				expect(features1[11].confidence).to.equal('1');
				expect(features1[11].frame).to.equal('cellar');
			});
			it('The first cellar uri inserted of the first document should be "http://publications.europa.eu/resource/cellar/8ebca567-705a-4a44-8cf7-f6a670213c0f" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features1[12].levels.values).to.equal('http://publications.europa.eu/resource/cellar/8ebca567-705a-4a44-8cf7-f6a670213c0f');
				expect(features1[12].confidence).to.equal('1');
				expect(features1[12].frame).to.equal('cellar');
			});
			it('The case number of the second document should be C-20/05  and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features2[5].levels.values).to.equal('C-20/05');
				expect(features2[5].confidence).to.equal('1');
				expect(features2[5].frame).to.equal('cellar');
			});
			it('The case year of the second document should be 2005 and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features2[6].levels.values).to.equal('2005');
				expect(features2[6].confidence).to.equal('1');
				expect(features2[6].frame).to.equal('cellar');
			});
			it('The sector of the second document should be 6 and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features2[7].levels.values).to.equal('6');
				expect(features2[7].confidence).to.equal('1');
				expect(features2[7].frame).to.equal('cellar');
			});
			it('The date of the second document should be "2007-06-28" and it should have confidence equal to 1. The frame should be equal to "cellar"', () => {
				expect(features2[8].levels[0].values).to.equal('2007');
				expect(features2[8].levels[1].values).to.equal('06');
				expect(features2[8].levels[2].values).to.equal('28');
				expect(features2[8].confidence).to.equal('1');
				expect(features2[8].frame).to.equal('cellar');
			});
			it('The first document type inserted of the second document should be "cdm:document_cjeu" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[9].levels.values).to.equal('cdm:document_cjeu');
				expect(features2[9].confidence).to.equal('1');
				expect(features2[9].frame).to.equal('cellar');
			});
			it('The second document type inserted of the second document should be "cdm:opinion_advocate-general" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[10].levels.values).to.equal('cdm:opinion_advocate-general');
				expect(features2[10].confidence).to.equal('1');
				expect(features2[10].frame).to.equal('cellar');
			});
			it('The second celex inserted of the second document should be "62005CC0020" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[11].levels.values).to.equal('62005CC0020');
				expect(features2[11].confidence).to.equal('1');
				expect(features2[11].frame).to.equal('cellar');
			});
			it('The second ecli inserted of the second document should be "ECLI:EU:C:2007:387" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[12].levels.values).to.equal('ECLI:EU:C:2007:387');
				expect(features2[12].confidence).to.equal('1');
				expect(features2[12].frame).to.equal('cellar');
			});
			it('The second cellar uri inserted of the second document should be "http://publications.europa.eu/resource/cellar/eb865324-7df6-49b8-9723-93287b97121b" and its confidence should be equal to "1". The frame should be equal to "cellar"', () => {
				expect(features2[13].levels.values).to.equal('http://publications.europa.eu/resource/cellar/eb865324-7df6-49b8-9723-93287b97121b');
				expect(features2[13].confidence).to.equal('1');
				expect(features2[13].frame).to.equal('cellar');
			});
		});
	});
});
