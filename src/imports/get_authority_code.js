import _ from 'underscore';
import getAuthoritiesList from './get_authorities_list.js';

/**
* This function is used adjust the authority by matching the one received with its correspondant in the cdm onotlogy
* @param {String} authority The authority type that must be adjusted
* @param {Object} configs The configuration of the library
* @return {String} The adjusted authority
*/
const getAuthorityCode = (authority, configs) => {
	const regex = new RegExp(authority, 'gi');
	const authorities = getAuthoritiesList(configs);
	const neededAuthority = _.filter(authorities, (obj) => {
		const codes = obj.codes;
		for (const code of codes) {
			if (code.match(regex)) {
				return code;
			}
		}
	});
	if (neededAuthority.length === 0) {
		return _.pluck(
			authorities,
			configs.global.controls.configJsonValueName
		);
	}
	if (neededAuthority.length === 1) {
		return [neededAuthority[0].caseNumberCode];
	}
	return [_.where(authorities, {
		code: neededAuthority
	})];
};

export default getAuthorityCode;
