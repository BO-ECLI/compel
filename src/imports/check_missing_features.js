import _ from 'underscore';
import getFeatures from './get_features.js';

/**
 * This function receives a JSON-LD and check if there are missing ones
 * @param {Array} features The array of features that must be checked
	* @param {Object} configs The configuration of the library
 * @return {string} Returns an object containing info about missing features
*/
const checkMissingFeatures = (aJsonLd, configs) => {
	const returnObject = [];
	for (const value of configs.checkMissingFeatures.fieldsToCheck) {
		if (_.where(getFeatures(aJsonLd), { name: value }).length === 0) {
			returnObject.push(value);
		}
	}
	return returnObject;
};

export default checkMissingFeatures;
