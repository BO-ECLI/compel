/**
 * This function extracts the document type name by starting by a document type formatted as an URL
 * @param {String} aTypeUrl the URL of the document type
 * @param {Object} configs The configuration of the library
 * @return {String} The document type as string
 */
const getDocumentTypeFromURL = (aTypeUrl, configs) => {
	const docTypeWithoutHas =
		aTypeUrl.split(configs.global.documentTypesHash)[1];
	return `${configs.global.cdmPrefix}${docTypeWithoutHas}`;
};

export default getDocumentTypeFromURL;
