import _ from 'underscore';
import sendSprqlQuery from './send_sprql_query.js';
import getFeatureValue from './get_feature_value.js';
import getFeature from './get_feature.js';
import setFeature from './set_feature.js';
import createSectorFeature from './create_sector_feature.js';
import createDateDocFeature from './create_date_doc_feature.js';
import createTypeFeature from './create_type_feature.js';
import createCaseNumberFeature from './create_case_number_feature.js';
import createCaseYearFeature from './create_case_year_feature.js';
import createCelexFeature from './create_celex_feature.js';
import createEcliFeature from './create_ecli_feature.js';
import createCellarUriFeature from './create_cellar_uri_feature.js';
import checkMissingFeatures from './check_missing_features.js';
import getCompleteCaseNumber from './get_complete_case_number.js';
import getAuthorityCode from './get_authority_code.js';
import getDocNames from './get_doc_names.js';
import getDocumentTypeFromURL from './get_document_type_from_url.js';

/**
* This function receives a JSON-LD in the {@link references.object.jsonLd} format and completes it if any feature is missing
* @param {Object} aJsonLd The JSON-LD that must be completed
* @param {Object} configs The configuration of the library
* @return {Object} A completed JSON-LD in the {@link references.object.jsonLd} format
*/
const fillMissingFeatures = async (aJsonLd, configs) => {
	const missingValues = checkMissingFeatures(
		aJsonLd,
		configs
	);
	const caseNumber = getFeatureValue(
		aJsonLd,
		configs.global.features.caseNumber
	);
	const caseYear = getFeatureValue(
		aJsonLd,
		configs.global.features.caseYear
	);
	const docType = getFeatureValue(
		aJsonLd,
		configs.global.features.type
	);
	const authority = getFeatureValue(
		aJsonLd,
		configs.global.features.authority
	);
	const authorityCode = getAuthorityCode(authority, configs);

	const titleRegex =
	(!missingValues.includes(configs.global.features.caseNumber))
	?
		getCompleteCaseNumber(
			caseNumber,
			caseYear,
			authorityCode,
			configs
		)
	:
		getFeatureValue(
			aJsonLd,
			configs.global.features.caseParty
		);

	const docNames = getDocNames(docType, configs);

	const queryResults = await sendSprqlQuery(
		docNames,
		titleRegex
	);
	const resultsFromSPRQL = queryResults.results.bindings;
	const resultDocuments = [];
	for (const document of resultsFromSPRQL) {
		const newJsonLd = (JSON.parse(JSON.stringify(aJsonLd)));
		const documentDate = document.date.value;
		const documentTypes = document.workTypes.value.split(
			configs.global.documentTypesSeparator
		);
		const documentWorkIds = document.workIds.value.split(
			configs.global.documentIdsSeparator
		).map(item => (
			(item.split(configs.global.celexPrefix).length === 2)
				? item.split(configs.global.celexPrefix)[1]
				: item.split(configs.global.ecliPrefix)[1]
		));
		const documentCellarUris = document.cellarURIs.value.split(
			configs.global.documentCellarUrisSeparator
		);
		const caseNumberFeatureName = configs.global.features.caseNumber;
		const documentCellarCaseNumber =
				document.title.value.match(/[A-Za-z]+-\d+\/[^\.]+/gi)[0];
		if (missingValues.includes(caseNumberFeatureName)) {
			const newCaseNumberFeature = createCaseNumberFeature(
				documentCellarCaseNumber,
				configs
			);
			setFeature(newJsonLd, newCaseNumberFeature);
		}
		const yearFeatureName = configs.global.features.caseYear;
		if (missingValues.includes(yearFeatureName)) {
			let documentCellarCaseYear = documentCellarCaseNumber.split(
				configs.global.yearSeparator
			)[1];
			const yearBeforeTwoThousands =
				configs.global.yearBeforeTwoThousands;
			const yearAfterTwoThousands =
				configs.global.yearAfterTwoThousands;

			documentCellarCaseYear =
				(documentCellarCaseYear[0] ===
					configs.global.yearFromCellarFirstDigitCheck)
				?
					`${yearAfterTwoThousands}${documentCellarCaseYear}`
				:
					`${yearBeforeTwoThousands}${documentCellarCaseYear}`;

			const newCaseYearFeature = createCaseYearFeature(
				documentCellarCaseYear,
				configs
			);
			setFeature(newJsonLd, newCaseYearFeature);
		}
		const typeFeatureName = configs.global.features.type;
		const sectorFeatureName = configs.global.features.sector;
		const dateDocFeatureName = configs.global.features.dateDoc;
		const newSectorFeature = (
			missingValues.includes(sectorFeatureName
		))
			? createSectorFeature(configs)
			: null;
		setFeature(newJsonLd, newSectorFeature);
		const dateDocFromCellar = createDateDocFeature(
			documentDate,
			configs
		);
		const newDateDocFeature = (
			missingValues.includes(dateDocFeatureName)
		)
			? dateDocFromCellar
			: (() => {
				const dateDocFromJsonLdText = getFeatureValue(
					newJsonLd,
					configs.global.features.dateDoc
				).join(configs.global.dateFieldsSeparator);
				const dateDocFromCellarText = _.pluck(
					dateDocFromCellar.levels,
					configs.global.controls.configJsonValuesName
				).join(configs.global.dateFieldsSeparator);
				return (dateDocFromJsonLdText === dateDocFromCellarText)
					? (() => {
						const dateDocFromJsonLd = getFeature(
							newJsonLd,
							configs.global.features.dateDoc
						);
						dateDocFromJsonLd.confidence =
							configs.global.confidenceFromCellar;
					})()
					: (() => {
						const dateDocFromJsonLd = getFeature(
							newJsonLd,
							configs.global.features.dateDoc
						);
						dateDocFromJsonLd.confidence =
							configs.global.confidenceReplacedByCellar;
						return dateDocFromCellar;
					})();
			})();
		setFeature(newJsonLd, newDateDocFeature);
		const newTypeDocFeatures = (
			missingValues.includes(typeFeatureName)
		)
			? (() => {
				const typesFromCellar = [];
				for (const type of documentTypes) {
					typesFromCellar.push(
						createTypeFeature(
							getDocumentTypeFromURL(type, configs),
							configs
						)
					);
				}
				return typesFromCellar;
			})()
			: (() => {
				const typesFromCellar = [];
				for (const type of documentTypes) {
					typesFromCellar.push(
						createTypeFeature(
							getDocumentTypeFromURL(type, configs),
							configs
						)
					);
				}
				const typeFromJsonLd = getFeature(
					newJsonLd,
					configs.global.features.type
				);
				typeFromJsonLd.confidence =
					configs.global.confidenceReplacedByCellar;
				return typesFromCellar;
			})();
		for (const newTypeFeature of newTypeDocFeatures) {
			setFeature(
				newJsonLd,
				newTypeFeature
			);
		}
		if (documentWorkIds.length === 1) {
			const newCelexFeature = createCelexFeature(
				documentWorkIds[0],
				configs
			);
			setFeature(newJsonLd, newCelexFeature);
		} else if (documentWorkIds.length === 2) {
			const newCelexFeature = createCelexFeature(
				documentWorkIds[0],
				configs);
			const newEcliFeature = createEcliFeature(
				documentWorkIds[1],
				configs);
			setFeature(newJsonLd, newCelexFeature);
			setFeature(newJsonLd, newEcliFeature);
		}

		for (const cellarUri of documentCellarUris) {
			const newCellarUriFeature = createCellarUriFeature(
				cellarUri,
				configs
			);
			setFeature(newJsonLd, newCellarUriFeature);
		}
		resultDocuments.push(newJsonLd);
	}
	return resultDocuments;
};

export default fillMissingFeatures;
