import _ from 'underscore';
import getFeatures from './get_features.js';

/**
 * This function receives a set of features and returns the requested feature
 * @param {Array} features The array of features that must be checked
 * @param {String} neededFeature The feature that must be retreived
 * @return {Object} Returns an object containing the values of the needed feature
*/
const getFeatureValue = (aJsonLd, neededFeature) => {
	const feature = _.where(getFeatures(aJsonLd), {
		name: neededFeature
	});
	return (feature[0])
		? (() => (
			(typeof feature[0].levels.values === 'string')
				? feature[0].levels.values
				: _.pluck(feature[0].levels, 'values')
		))()
		: null;
};

export default getFeatureValue;
