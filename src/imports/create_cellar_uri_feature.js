/**
 	* This function creates a cellarUri features and returns it.
	* @param {String} cellarUri The cellarUri to insert in the new feature
	* @param {Object} configs The configuration of the library
	* @return {Object} The created cellarUri feature
*/
const createCellarUriFeature = (cellarUri, configs) => {
	const nameName = configs.createCellarUriFeature.cellarUriName.name;
	const nameValue = configs.createCellarUriFeature.cellarUriName.value;
	const frameName = configs.createCellarUriFeature.cellarUriFrame.name;
	const frameValue = configs.createCellarUriFeature.cellarUriFrame.value;
	const FRBRName = configs.createCellarUriFeature.cellarUriFRBR.name;
	const FRBRValue = configs.createCellarUriFeature.cellarUriFRBR.value;
	const cellarUriLevelsName =
		configs.createCellarUriFeature.cellarUriLevels.name;
	const confidenceName =
		configs.createCellarUriFeature.cellarUriConfidence.name;
	const cellarUriObject = {
		[nameName]: nameValue,
		[frameName]: frameValue,
		[FRBRName]: FRBRValue,
		[confidenceName]: configs.global.confidenceFromCellar,
		[cellarUriLevelsName]: { values: cellarUri },
	};
	return cellarUriObject;
};

export default createCellarUriFeature;
