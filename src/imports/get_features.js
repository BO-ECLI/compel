/**
 * This function returns all the features contained in a JSON-LD
 * @param {Object} aJsonLd The JSON-LD whose features must be extracted
 * @return {Objects} Returns all the features of the given JSON-LD
*/
const getFeatures = aJsonLd => (
	aJsonLd.references[0].features
);

export default getFeatures;
