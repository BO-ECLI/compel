/**
 * This function returns an array containing all the possible authorities
 * @param {Object} configs The configuration of the library
 * @return {Array} An array containing all the possible doc types
*/
const getAuthoritiesList = configs => (
	configs.getAuthoritiesList.authorities
);

export default getAuthoritiesList;
