/**
	* This function returns an array containing all the possible sector to create a complete JSON-LD
 	* @return {Array} Returns an array containing all the possible sectors
*/
const getSectors = (configs) => {
	const returnObject = [];
	for (const value of configs.getSectors.sectors) {
		returnObject.push(value);
	}
	return returnObject;
};

/**
 	* This function creates a sector features and returns it.
	* @param {Object} configs The configuration of the library
	* @return {Object} The created sector feature
*/
const createSectorFeature = (configs) => {
	const possibleSectors = getSectors(configs);
	const nameName = configs.createSectorFeature.sectorName.name;
	const nameValue = configs.createSectorFeature.sectorName.value;
	const frameName = configs.createSectorFeature.sectorFrame.name;
	const frameValue = configs.createSectorFeature.sectorFrame.value;
	const FRBRName = configs.createSectorFeature.sectorFRBR.name;
	const FRBRValue = configs.createSectorFeature.sectorFRBR.value;
	const sectorLevelsName = configs.createSectorFeature.sectorLevels.name;
	const confidenceName = configs.createSectorFeature.sectorConfidence.name;
	const sectorObject = {
		[nameName]: nameValue,
		[frameName]: frameValue,
		[FRBRName]: FRBRValue,
		[confidenceName]: configs.global.confidenceFromCellar,
		[sectorLevelsName]:
			(possibleSectors.length === 1)
				? { values: possibleSectors[0] }
				: (() => {
					const valuesArray = [];
					for (const value of possibleSectors) {
						valuesArray.push({ values: value });
					}
					return valuesArray;
				})(),
	};
	return sectorObject;
};

export default createSectorFeature;
