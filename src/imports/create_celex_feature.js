/**
 	* This function creates a celex features and returns it.
	* @param {String} celex The celex to insert in the new feature
	* @param {Object} configs The configuration of the library
	* @return {Object} The created celex feature
*/
const createCelexFeature = (celex, configs) => {
	const nameName = configs.createCelexFeature.celexName.name;
	const nameValue = configs.createCelexFeature.celexName.value;
	const frameName = configs.createCelexFeature.celexFrame.name;
	const frameValue = configs.createCelexFeature.celexFrame.value;
	const FRBRName = configs.createCelexFeature.celexFRBR.name;
	const FRBRValue = configs.createCelexFeature.celexFRBR.value;
	const celexLevelsName = configs.createCelexFeature.celexLevels.name;
	const confidenceName = configs.createCelexFeature.celexConfidence.name;
	const celexObject = {
		[nameName]: nameValue,
		[frameName]: frameValue,
		[FRBRName]: FRBRValue,
		[confidenceName]: configs.global.confidenceFromCellar,
		[celexLevelsName]: { values: celex },
	};
	return celexObject;
};

export default createCelexFeature;
