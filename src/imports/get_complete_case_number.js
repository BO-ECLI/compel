/**
* This function is used adjust the case number by appending to it the last two characters of the doc-year feature
* @param {String} caseNumber The case number that must be adjusted
* @param {String} caseYear The case year whose last two characters must be appended to the case number
* @param {String} courtCode The court code in eur-lex
* @param {String} configs The configurations of the library
* @return {String} The adjusted case number
*/
const getCompleteCaseNumber = (caseNumber, caseYear, courtCode, configs) => {
	if (!caseYear || !caseNumber || !courtCode) {
		return caseNumber;
	}
	const newCaseNumber = (caseNumber.includes(configs.global.yearSeparator))
		? caseNumber
		: `${caseNumber}${configs.global.yearSeparator}${caseYear.slice(2, 4)}`;
	return (newCaseNumber.includes(configs.global.courtCodeSeparator))
		? newCaseNumber
		: `${courtCode}${configs.global.courtCodeSeparator}${newCaseNumber}`;
};

export default getCompleteCaseNumber;
