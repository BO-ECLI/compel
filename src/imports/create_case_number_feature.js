/**
 	* This function creates a case number features and returns it.
	* @param {String} caseNumber The CaseNumber to insert in the new feature
	* @param {Object} configs The configuration of the library
	* @return {Object} The created CaseNumber feature
*/
const createCaseNumberFeature = (caseNumber, configs) => {
	const nameName = configs.createCaseNumberFeature.caseNumberName.name;
	const nameValue = configs.createCaseNumberFeature.caseNumberName.value;
	const frameName = configs.createCaseNumberFeature.caseNumberFrame.name;
	const frameValue = configs.createCaseNumberFeature.caseNumberFrame.value;
	const FRBRName = configs.createCaseNumberFeature.caseNumberFRBR.name;
	const FRBRValue = configs.createCaseNumberFeature.caseNumberFRBR.value;
	const caseNumberLevelsName =
		configs.createCaseNumberFeature.caseNumberLevels.name;
	const confidenceName =
		configs.createCaseNumberFeature.caseNumberConfidence.name;
	const caseNumberObject = {
		[nameName]: nameValue,
		[frameName]: frameValue,
		[FRBRName]: FRBRValue,
		[confidenceName]: configs.global.confidenceFromCellar,
		[caseNumberLevelsName]: { values: caseNumber },
	};
	return caseNumberObject;
};

export default createCaseNumberFeature;
