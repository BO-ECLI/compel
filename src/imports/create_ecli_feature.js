/**
 	* This function creates a ecli features and returns it.
	* @param {String} ecli The ecli to insert in the new feature
	* @param {Object} configs The configuration of the library
	* @return {Object} The created ecli feature
*/
const createEcliFeature = (ecli, configs) => {
	const nameName = configs.createEcliFeature.ecliName.name;
	const nameValue = configs.createEcliFeature.ecliName.value;
	const frameName = configs.createEcliFeature.ecliFrame.name;
	const frameValue = configs.createEcliFeature.ecliFrame.value;
	const FRBRName = configs.createEcliFeature.ecliFRBR.name;
	const FRBRValue = configs.createEcliFeature.ecliFRBR.value;
	const ecliLevelsName = configs.createEcliFeature.ecliLevels.name;
	const confidenceName = configs.createEcliFeature.ecliConfidence.name;
	const ecliObject = {
		[nameName]: nameValue,
		[frameName]: frameValue,
		[FRBRName]: FRBRValue,
		[confidenceName]: configs.global.confidenceFromCellar,
		[ecliLevelsName]: { values: ecli },
	};
	return ecliObject;
};

export default createEcliFeature;
