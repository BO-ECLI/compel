/**
	* This function creates a date-doc features and returns it.
	* @param {String} date The date to insert in the new feature
	* @param {Object} configs The configuration of the library
	* @return {Object} The created date-doc feature
*/
const createDateDocFeature = (date, configs) => {
	const nameName = configs.createDateDocFeature.dateDocName.name;
	const nameValue = configs.createDateDocFeature.dateDocName.value;
	const frameName = configs.createDateDocFeature.dateDocFrame.name;
	const frameValue = configs.createDateDocFeature.dateDocFrame.value;
	const FRBRName = configs.createDateDocFeature.dateDocFRBR.name;
	const FRBRValue = configs.createDateDocFeature.dateDocFRBR.value;
	const dateDocLevelsName = configs.createDateDocFeature.dateDocLevels.name;
	const confidenceName = configs.createDateDocFeature.dateDocConfidence.name;
	const dateDocObject = {
		[nameName]: nameValue,
		[frameName]: frameValue,
		[FRBRName]: FRBRValue,
		[confidenceName]: configs.global.confidenceFromCellar,
		[dateDocLevelsName]:
			(() => {
				const valuesArray = [];
				const dateArray = date.split(
					configs.global.dateFieldsSeparator
				);
				for (const token of dateArray) {
					valuesArray.push({ values: token });
				}
				return valuesArray;
			})(),
	};
	return dateDocObject;
};

export default createDateDocFeature;
