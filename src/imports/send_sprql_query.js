/* eslint-disable max-len */
import { SparqlClient } from 'sparql-client-2';
import configs from '../configs/configs.json';

/**
	* This function sends the SPRQL query that must be sent to the eur-lex endpoint and return the results
	* @param {String} docType the doc types that must be retrieved
	* @param {String} caseNumber the case number of the documents that must be retrieved
	* @return {Object} The JSON-LD in the {@link references.object.jsonLd} format completed with the date-doc feature
*/
const sendSprqlQuery = (docTypes = [], caseNumber = '') => (
	new Promise((resolve, reject) => {
		const endpoint = configs.global.sparqlEndpoint;
		const client = new SparqlClient(endpoint);
		//# 2017 - CIRSFID, University of Bologna.
		//# This query is licensed under the terms of the CC-BY 4.0 license (https://creativecommons.org/licenses/by-sa/4.0/)
		//# Authors: Luca Cervone, Monica Palmirani.
		const query = `
			PREFIX cdm:<http://publications.europa.eu/ontology/cdm#>
			PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
			PREFIX dc:<http://purl.org/dc/elements/1.1/>
			PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
			PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX owl:<http://www.w3.org/2002/07/owl#>
			SELECT DISTINCT
				(group_concat(distinct ?work;separator=",") as ?cellarURIs)
				(group_concat(distinct ?title_;separator=",") as ?title)
				?langIdentifier
				(group_concat(distinct ?resType;separator=",") as ?workTypes)
				(group_concat(distinct ?agentName;separator=",") as ?authors)
				?date
				(group_concat(distinct ?workId_;separator=",") as ?workIds)

			WHERE {
				?work cdm:work_date_document ?d.
				?exp cdm:expression_belongs_to_work ?work .
				?exp cdm:expression_title ?title_ .
				?exp cdm:expression_uses_language/dc:identifier ?langIdentifier .
				?work rdf:type ?resType .
				OPTIONAL {?work cdm:work_created_by_agent/skos:prefLabel ?agentName .
				filter (lang(?agentName)="en")}.
				?work cdm:work_date_document ?date .
				?work cdm:work_id_document ?workId_ .
				?exp cdm:expression_uses_language <http://publications.europa.eu/resource/authority/language/ENG>.
				FILTER ( ( regex( str(?title_), "${caseNumber}", "i")) &&  (${
					(() => {
						const fixedArray = docTypes.map(x => (`?resType = ${x}`));
						return fixedArray.join(' || ');
					})()
				})).
			}

			GROUP BY ?work ?date ?langIdentifier
			LIMIT 100
			OFFSET 0`;
		client
			.query(query)
			.execute()
			.then((results) => {
				resolve(results);
			})
			.catch((error) => {
				reject(error);
			});
	})
);

export default sendSprqlQuery;
