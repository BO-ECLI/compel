/**
 * This function returns an array containing all the possible doc types
 * @param {Object} configs The configuration of the library
 * @return {Array} An array containing all the possible doc types
*/
const getDocTypesList = configs => (
	configs.getDocTypesList.types
);

export default getDocTypesList;
