/**
 * This function extracts the document year by starting by a document case number
 * @param {String} aCaseNumber the case number of the document
 * @param {Object} configs The configuration of the library
 * @return {String} The document year as string
 */
const getYearFromCaseNumber = (aCaseNumber, configs) => {
	const docTypeWithoutHas =
		aCaseNumber.split(configs.global.documentTypesHash)[1];
	return `${configs.global.cdmPrefix}${docTypeWithoutHas}`;
};

export default getYearFromCaseNumber;
