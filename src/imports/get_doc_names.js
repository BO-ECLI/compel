import _ from 'underscore';
import getDocTypesList from './get_doc_types_list.js';

/**
* This function is used adjust the doc type by matching the one received with its correspondant in the cdm onotlogy
* @param {String} caseType The case type that must be adjusted
* @param {Object} configs The configuration of the library
* @return {String} The adjusted case type
*/
const getDocNames = (caseType, configs) => {
	const regex = new RegExp(caseType, 'gi');
	const docTypes = getDocTypesList(configs);
	const neededDocType = _.filter(docTypes, (obj) => {
		const codes = obj.codes;
		for (const code of codes) {
			if (code.match(regex)) {
				return code;
			}
		}
	});
	if (neededDocType.length === 0) {
		return _.pluck(docTypes, configs.global.controls.configJsonValueName);
	}
	if (neededDocType.length === 1) {
		return [neededDocType[0].name];
	}
	return [_.where(docTypes, {
		code: neededDocType
	})];
};

export default getDocNames;
