/**
 * This function receives a JSON-LD and a feature and inserts the new feature in the JSON-LD
 * @param {Object} aJsonLd The JSON-LD that updated
 * @param {Object} aFeature The feature that must be inserted in the JSON-LD
 * @return {Object} Returns the updated JSON-LD
*/
const setFeature = (aJsonLd, aFeature) => (
	(aFeature)
	?
		aJsonLd.references[0].features.push(aFeature)
	:
		aJsonLd
);

export default setFeature;
