import _ from 'underscore';
import getFeatures from './get_features.js';

/**
 * This function receives a set of features and returns the requested feature
 * @param {Array} aJsonLd The JSON-LD that must be checked
 * @param {String} neededFeature The feature that must be retreived
 * @return {Object} Returns an object containing the values of the needed feature
*/
const getFeature = (aJsonLd, neededFeature) => (
	_.where(getFeatures(aJsonLd), {
		name: neededFeature
	})[0]
);

export default getFeature;
