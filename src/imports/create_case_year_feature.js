/**
 	* This function creates a case year features and returns it.
	* @param {String} caseYear The case year to insert in the new feature
	* @param {Object} configs The configuration of the library
	* @return {Object} The created case year feature
*/
const createCaseYearFeature = (caseYear, configs) => {
	const nameName = configs.createCaseYearFeature.caseYearName.name;
	const nameValue = configs.createCaseYearFeature.caseYearName.value;
	const frameName = configs.createCaseYearFeature.caseYearFrame.name;
	const frameValue = configs.createCaseYearFeature.caseYearFrame.value;
	const FRBRName = configs.createCaseYearFeature.caseYearFRBR.name;
	const FRBRValue = configs.createCaseYearFeature.caseYearFRBR.value;
	const caseYearLevelsName =
		configs.createCaseYearFeature.caseYearLevels.name;
	const confidenceName =
		configs.createCaseYearFeature.caseYearConfidence.name;
	const caseYearObject = {
		[nameName]: nameValue,
		[frameName]: frameValue,
		[FRBRName]: FRBRValue,
		[confidenceName]: configs.global.confidenceFromCellar,
		[caseYearLevelsName]: { values: caseYear },
	};
	return caseYearObject;
};

export default createCaseYearFeature;
