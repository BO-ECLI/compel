import _ from 'underscore';

/**
 * This function receives a JSON-LD and a feature and deletes feature from the JSON-LD
 * @param {Object} aJsonLd The JSON-LD that must be updated
 * @param {Object} aFeature The feature that must be deleted in the JSON-LD
 * @return {Object} Returns the updated JSON-LD
*/
const deleteFeature = (aJsonLd, aFeature) => {
	const updatedJsonLd =
		_.omit(
			aJsonLd,
			_.where(
				aJsonLd,
				{ name: aFeature }
			)
		);
	return updatedJsonLd;
};

export default deleteFeature;
