/**
	* This function creates a type features and returns it.
	* @param {Object} configs The configuration of the library
	* @return {Object} The created date-doc feature
*/
const createTypeFeature = (type, configs) => {
	const nameName = configs.createTypeFeature.typeName.name;
	const nameValue = configs.createTypeFeature.typeName.value;
	const frameName = configs.createTypeFeature.typeFrame.name;
	const frameValue = configs.createTypeFeature.typeFrame.value;
	const FRBRName = configs.createTypeFeature.typeFRBR.name;
	const FRBRValue = configs.createTypeFeature.typeFRBR.value;
	const typeLevelsName = configs.createTypeFeature.typeLevels.name;
	const confidenceName = configs.createTypeFeature.typeConfidence.name;
	const typeObject = {
		[nameName]: nameValue,
		[frameName]: frameValue,
		[FRBRName]: FRBRValue,
		[confidenceName]: configs.global.confidenceFromCellar,
		[typeLevelsName]: { values: type },
	};
	return typeObject;
};

export default createTypeFeature;
