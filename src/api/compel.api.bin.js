#!/usr/bin/env node
import program from 'commander';
import fs from 'fs';
import { log } from 'console';
import packageConfigs from '../package.json';
import Compel from './compel.api.min.js';

const compel = new Compel();

const isFile = (file) => {
	try {
		fs.accessSync(file, fs.F_OK);
		const stats = fs.statSync(file);
		if (stats.isFile()) {
			return true;
		}
		return false;
	} catch (e) {
		return e;
	}
};

const checkMissingFeatures = (file) => { //eslint-disable-line no-unused-vars
	if (isFile(file)) {
		const jsonStream = fs.readFileSync(file);
		const jsonLd = JSON.parse(jsonStream.toString());

		const result = compel.checkMissingFeatures(jsonLd);
		log(result.toString());
		return result;
	}
};

const fillMissingFeatures = async (file) => { //eslint-disable-line no-unused-vars
	if (isFile(file)) {
		const jsonStream = fs.readFileSync(file);
		const jsonLd = JSON.parse(jsonStream.toString());

		const result = await compel.fillMissingFeatures(jsonLd);
		log(JSON.stringify(result));
		return result;
	}
};

program
   .version(packageConfigs.version);
program
   .command('check-missing-features <file>')
   .description('Checks the missing features in the given JSON-LD') //eslint-disable-line max-len
   .action(checkMissingFeatures);
program
	.command('fill-missing-features <file>')
	.description('Completes the missing features in the given JSON-LD') //eslint-disable-line max-len
	.action(fillMissingFeatures);

program.parse(process.argv);
if (program.args.length === 0) program.help();
