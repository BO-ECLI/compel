import checkMissingFeatures from '../imports/check_missing_features.js';
import fillMissingFeatures from '../imports/fill_missing_features.js';
import errors from '../configs/errors.json';
import configs from '../configs/configs.json';


/**
	* The object that is returned by the check function
	* @typedef {Object} results.function.check
	* @property {Boolean} authority True if the authority feature is present
	* @property {Boolean} type True if the type feature is present
	* @property {Boolean} date True if the date feature is present
	* @property {Boolean} case-number True if the case-number feature is present
	* @property {Boolean} case-year True if the case-year feature is present
	* @property {Boolean} section True if the section feature is present
	* @property {Boolean} is-case-law-reference True if the is-case-law-reference feature is present
	* @property {Boolean} is-legal-reference True if the is-legal-reference feature is present
	*
	* @example
	* {
	* 		"authority": false,
	* 		"type": true,
	* 		"date": true,
	* 		"case-number": false,
	* 		"case-year": true,
	* 		"is-case-law-reference": true,
	* 		"is-legal-reference": true,
	* 		"section": true
	* }
*/
/**
 * The JSON-LD object for judgements references
	* @typedef {Object} references.object.jsonLd
	* @example
	*	{
	*	 "@context": [
	*	  "http://www.fabiovitali.it/legalcitem/lcm-context7.json",
	*	  "http://www.fabiovitali.it/legalcitem/lcm-local.json"
	*	 ],
	*	 "citation": {
	*	  "details": {
	*	   "text": "Corte di giustizia Europea, 12 giugno 2003, causa C-112/00"
	*	  }
	*	 },
	*	 "references": [
	*	  {
	*	   "features": [
	*	    {
	*	     "name": "authority",
	*	     "frame": "interpretation",
	*	     "frbr": "work",
	*	     "levels": {
	*	      "values": "CJEU"
	*	     }
	*	    },
	*	    {
	*	     "name": "type",
	*	     "frame": "source",
	*	     "frbr": "work",
	*	     "levels": {
	*	      "values": "CJ"
	*	     }
	*	    },
	*	    {
	*	     "name": "date-pub",
	*	     "frame": "extra",
	*	     "frbr": "work",
	*	     "levels": [
	*	      {
	*	       "values": "2003"
	*	      },
	*	      {
	*	       "values": "06"
	*	      },
	*	      {
	*	       "values": "12"
	*	      }
	*	     ]
	*	    },
	*	    {
	*	     "name": "case-number",
	*	     "frame": "source",
	*	     "frbr": "work",
	*	     "levels": {
	*	      "values": "C-112"
	*	     }
	*	    },
	*	    {
	*	     "name": "case-year",
	*	     "frame": "interpretation",
	*	     "frbr": "work",
	*	     "author": {
	*	      "fullname": "BOECLI eu.boecli.service.recognition.impl.DefaultEuropeanCaseLawReferenceRecognition \\\"Generic reference recognition to European case-law\\\" by ITTIG (v. 0.1) for language DEFAULT and jurisdiction DEFAULT in extension Default Extension"
	*	     },
	*	     "levels": {
	*	      "values": "2000"
	*	     }
	*	    },
	*	    {
	*	     "name": "is-case-law-reference",
	*	     "frame": "interpretation",
	*	     "frbr": "expression",
	*	     "author": {
	*	      "fullname": "BOECLI eu.boecli.service.recognition.impl.DefaultEuropeanCaseLawReferenceRecognition \\\"Generic reference recognition to European case-law\\\" by ITTIG (v. 0.1) for language DEFAULT and jurisdiction DEFAULT in extension Default Extension"
	*	     },
	*	     "levels": {
	*	      "values": true
	*	     }
	*	    },
	*	    {
	*	     "name": "is-legal-reference",
	*	     "frame": "interpretation",
	*	     "frbr": "expression",
	*	     "author": {
	*	      "fullname": "BOECLI eu.boecli.service.recognition.impl.DefaultEuropeanCaseLawReferenceRecognition \\\"Generic reference recognition to European case-law\\\" by ITTIG (v. 0.1) for language DEFAULT and jurisdiction DEFAULT in extension Default Extension"
	*	     },
	*	     "levels": {
	*	      "values": false
	*	     }
	*	    },
	*	    {
	*	     "name": "sector",
	*	     "frame": "cellar",
	*	     "frbr": "work",
	*	     "levels": {
	*	      "values": "6"
	*	     }
	*	    }
	*	   ]
	*	  }
	*	 ]
	*	}
*/

/**
	* The access point of the library.
*/
export default class Compel {

	constructor() {
		this.configs = configs;
		this.errorsMissingFeatures = errors.checkMissingFeatures;
		this.errorEFS = this.errorsMissingFeatures.emptyFeaturesSet;
		this.errorNMF = this.errorsMissingFeatures.nullMandatoryFeatures;
	}
	/**
	* This function receives a JSON-LD in the {@link references.object.jsonLd} format and checks if there are missing features
	* @throws {Error} throws emptyFeaturesSet error when the given set of features is empty
	* @param {Object} aJsonLd The JSON-LD that must be checked
	* @return {Object} An object structured as {@link results.function.check}
	*/
	checkMissingFeatures(aJsonLd) {
		return checkMissingFeatures(aJsonLd, this.configs);
	}

	/**
	* This function receives a JSON-LD in the {@link references.object.jsonLd} format and completes it if any feature is missing
	* @throws {Error} throws emptyFeaturesSet error when the given set of features is empty
	* @throws {Error} throws nullMandatoryFeatures error when the given set does not contain one or more mandatory feature
	* @param {Object} aJsonLd The JSON-LD that must be completed
	* @return {Object} A completed JSON-LD in the {@link references.object.jsonLd} format
	*/
	async fillMissingFeatures(aJsonLd) {
		try {
			return await fillMissingFeatures(aJsonLd, this.configs);
		} catch (e) {
			throw new Error(`${errors.errorFromSparqlQuery}\n${e}`);
		}
	}
}
