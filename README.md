# About #
compel is library to check if a JSON-LD is compliant to the format used for the BO-ECLI project.

# Usage #

##### Node.js #####

Install the library with the following command:

```bash
npm install --save compel;
```

If the library is not published on npm and you have downloaded te source code, just move in the library's folder and run the follwing commands:

```bash
npm install;
npm build;
```
Then move on you project folder and run

```bash
npm link compel;
```
Then use the library as the following:

```javascript
import Compel from 'compel';

const compel = new Compel();
const compelJsonLd = compel.fillMissingValues(aJsonLd);
```

##### Browser #####

Import the following in the script section of your HTML file:

```html
<script src="path/to/your/scripts/lib/folder/compel.api.min.js"></script>
```

Then use the library as the following:

```javascript
const compel = new Compel();
var results = compel.checkMissingValues(aJsonLd);
```

##### CLI #####
Install the library with the following command:

```bash
npm install -g compel;
```

If the library is not published on npm and you have downloaded te source code, just move in the library's folder and run the follwing commands:

```bash
npm install;
npm build;
```

Then use the library as the following:

```bash
compel [command] [filename]

compel check path/to/the/json/file/to/check
```
# Builds #

##### All builds #####
Run the following:

`npm run build`

##### Node build #####
Run the following:

`npm run build-node`

##### Browser build #####
Run the following:

`npm run build-browser`

##### Bin build #####
Run the following:

`npm run build-bin`

## Tests ##

##### All tests #####
Run the following:

`npm run test`

##### Node tests #####
Run the following:

`npm run test-node`

##### Browser tests #####
Run the following:

`npm run test-browser`

If your browser does not open after the above command finished its esecution, open your preferred browser and go to the following address: http://localhost:9000/test/test.html

##### Bin tests #####
Run the following:

`npm run test-bin`

# Contributing #

In lieu of a formal style guide, take care to maintain the existing coding style. Add at least 3 unit tests for any new functionality. Be sure that any changed functionality continue to pass its dedicated tests. Lint and test your code.
