### HOW TO CONTRIBUTE

If you want to add a new functionality on compel follow the following steps:

#### Install EditorConfig
   Before writing code or documentation you have to install the [EditorConfig](http://editorconfig.org/) plugin for your text editor or IDE. This plugin EditorConfig helps developers define and maintain consistent coding styles between different editors and IDEs.

#### Write Documentation
   Firstly you have to write the basic documentation in the file README.md:

   - write an example of the functionality you are going to develop in the file README.md, for node.js usage;
   - write an example of the functionality you are going to develop in the file README.md, for browser usage;
   - write an example of the functionality you are going to develop in the file README.md, for CLI usage.

#### Write Tests
   Secondly, you have to write tests for node, browser and CLI. Tests must be the same for each of the three possible usage of the library:

   - create at least three tests for each document in the file test.js (this is for node and browser tests);
   - create at least three tests (the same of previous bullet) for each document in the file test-bin.js (this is for CLI tests).

#### Write the entry points of the functionality you are going to develop
   Thirdly, you have to create two entry points in the API for node and browser usage and for CLI usage:

   - create the method "nameOfFunctionality" in the file api/compel.api.js;
   - create the method "nameOfFunctionality" in the file api/compel.api.bin.js
      - create the proper "program" method.

#### Write the complete documentation

   Fourtly, create the complete documentation for each one of the two entry points:

   - write the documentation compliant to esdoc of the method in api/compel.api.js (include examples for CLI and Browser);
   - write the complete documentation in the api/compel.api.bin.js. It will not be inserted in the documentation files but it is always appreciated if you write it

#### Develop your functionality

   Your functionality must always have an entry point in api/compel.api.js and api/compel.api.bin.js.

   If your functionality needs functions that are still not implemented in the library, create the file "name-of-function.js" in the imports folder, then implement your function there.

			If your functionality needs modified versions of functions that are already implemented in the library, just modify the implemented function and be sure that it still continues to pass its dedicated tests.

			If you need configurations constants or your if your function throws specific errors, insert them respectively in configs/configs.json and configs/errors.json.
